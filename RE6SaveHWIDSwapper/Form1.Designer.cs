﻿namespace RE6SaveHWIDSwapper
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnFrom = new System.Windows.Forms.Button();
            this.btnSelectTargetSave = new System.Windows.Forms.Button();
            this.txtHWID = new System.Windows.Forms.TextBox();
            this.lblHWID = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.txtStatus = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.patchBH6exeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnFrom
            // 
            this.btnFrom.Location = new System.Drawing.Point(7, 80);
            this.btnFrom.Name = "btnFrom";
            this.btnFrom.Size = new System.Drawing.Size(129, 23);
            this.btnFrom.TabIndex = 2;
            this.btnFrom.Text = "Get HWID From Original";
            this.btnFrom.UseVisualStyleBackColor = true;
            this.btnFrom.Click += new System.EventHandler(this.btnFrom_Click);
            // 
            // btnSelectTargetSave
            // 
            this.btnSelectTargetSave.Location = new System.Drawing.Point(142, 80);
            this.btnSelectTargetSave.Name = "btnSelectTargetSave";
            this.btnSelectTargetSave.Size = new System.Drawing.Size(124, 23);
            this.btnSelectTargetSave.TabIndex = 3;
            this.btnSelectTargetSave.Text = "Select Target Save";
            this.btnSelectTargetSave.UseVisualStyleBackColor = true;
            this.btnSelectTargetSave.Click += new System.EventHandler(this.btnSelectTargetSave_Click);
            // 
            // txtHWID
            // 
            this.txtHWID.Location = new System.Drawing.Point(99, 22);
            this.txtHWID.MaxLength = 16;
            this.txtHWID.Name = "txtHWID";
            this.txtHWID.Size = new System.Drawing.Size(122, 20);
            this.txtHWID.TabIndex = 1;
            this.txtHWID.TextChanged += new System.EventHandler(this.txtHWID_TextChanged);
            // 
            // lblHWID
            // 
            this.lblHWID.AutoSize = true;
            this.lblHWID.Location = new System.Drawing.Point(52, 25);
            this.lblHWID.Name = "lblHWID";
            this.lblHWID.Size = new System.Drawing.Size(40, 13);
            this.lblHWID.TabIndex = 7;
            this.lblHWID.Text = "HWID:";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(74, 109);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(129, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save Target To Original";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(52, 52);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(40, 13);
            this.lblStatus.TabIndex = 10;
            this.lblStatus.Text = "Status:";
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtStatus
            // 
            this.txtStatus.AutoSize = true;
            this.txtStatus.ForeColor = System.Drawing.Color.Red;
            this.txtStatus.Location = new System.Drawing.Point(96, 52);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(86, 13);
            this.txtStatus.TabIndex = 11;
            this.txtStatus.Text = "Need Target File";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(275, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.patchBH6exeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // patchBH6exeToolStripMenuItem
            // 
            this.patchBH6exeToolStripMenuItem.Name = "patchBH6exeToolStripMenuItem";
            this.patchBH6exeToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.patchBH6exeToolStripMenuItem.Text = "Patch BH6.exe";
            this.patchBH6exeToolStripMenuItem.Click += new System.EventHandler(this.patchBH6exeToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(275, 140);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblHWID);
            this.Controls.Add(this.txtHWID);
            this.Controls.Add(this.btnSelectTargetSave);
            this.Controls.Add(this.btnFrom);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(291, 179);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(291, 179);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RE6 Save Transfer By gir489";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFrom;
        private System.Windows.Forms.Button btnSelectTargetSave;
        private System.Windows.Forms.TextBox txtHWID;
        private System.Windows.Forms.Label lblHWID;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label txtStatus;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem patchBH6exeToolStripMenuItem;
    }
}

